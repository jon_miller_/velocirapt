const path = require('path');
const fs = require('fs');

const React = require('react');
const { Provider } = require('react-redux');
const { renderToString } = require('react-dom/server');
const { StaticRouter } = require('react-router-dom');

const { default: configureStore } = require('../src/store');
const { default: App } = require('../src/containers/App');

const store = configureStore();
module.exports = function universalLoader(req, res) {
  const filePath = path.resolve(__dirname, '..', 'build', 'index.html');

  fs.readFile(filePath, 'utf8', (err, htmlData) => {
    if (err) {
      console.error('read err', err);
      res.status(404).end();
    }

    const context = {};

    const markup = renderToString(
      <Provider store={store}>
        <StaticRouter
          location={req.url}
          context={context}
        >
          <App />
        </StaticRouter>
      </Provider>,
    );

    if (context.url) {
      // Somewhere a `<Redirect>` was rendered
      res.redirect(301, context.url);
    } else {
      let RenderedApp = htmlData.replace('{{SSR}}', markup);
      const preloadedState = `<script>window.__PRELOADED_STATE__=${JSON.stringify(store.getState()).replace(/</g, '\\u003c')}</script>`;
      const scriptTag = '<script type';
      RenderedApp = RenderedApp.replace(scriptTag, preloadedState + scriptTag);
      res.set('content-type', 'text/html');
      res.send(RenderedApp);
    }
  });
};


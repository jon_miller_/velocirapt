const express = require('express');

const router = express.Router();
router.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

router.get('/', (req, res) => {
  res.json({ item: 1, test: 2 });
});

router.get('/firstroute', (req, res) => {
  res.json({ routenumber: 1, test: 2 });
});

module.exports = router;


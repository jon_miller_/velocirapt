import reducer from './user';
import * as types from '../types/user';
import * as actions from '../actions/user';

const initialState = {
  email: 'user@example.com',
};

describe('todos reducer', () => {
  it('should return the initial state', () => {
    expect(
      reducer(undefined, {}),
    ).toEqual(
      initialState,
    );
  });

  it('should handle SET', () => {
    expect(
      reducer(initialState, actions.set({ tester: '1' })),
    ).toEqual(
      { email: 'user@example.com', tester: '1' },
    );
  });
});

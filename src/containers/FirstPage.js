import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import * as userActions from '../actions/user';
import PropTypes from 'prop-types';

import './FirstPage.css';

export const FirstPage = (props) => {
  // staticContext only relevant for server side
  const b64 = props.staticContext ? 'wait for it' : window.btoa('wait for it');
  return (
    <div className="bold">
      <h2>First Page</h2>
      <p>{`Email: ${props.user.email}`}</p>
      <p>{`b64: ${b64}`}</p>
      <Link to={'/second'}>Second</Link>
    </div>
  );
};

FirstPage.propTypes = {
  staticContext: PropTypes.object,
  user: PropTypes.shape({
    email: PropTypes.string.isRequired,
  }),
};

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  userActions: bindActionCreators(userActions, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FirstPage);

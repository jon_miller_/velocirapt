import React from 'react';
import { Switch, Route } from 'react-router-dom';
import FirstPage from './FirstPage';

import SecondPage from './SecondPage';

import NoMatch from '../components/NoMatch';

const App = () => (
  <div>
    <h1>TEST</h1>
    <Switch>
      <Route exact path="/" component={FirstPage} />
      <Route path="/second" component={SecondPage} />
      <Route component={NoMatch} />
    </Switch>
  </div>
);

export default App;

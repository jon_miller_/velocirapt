import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import { FirstPage } from './FirstPage';

describe('<FirstPage />', () => {
  it('renders with default props', () => {
    const elem = { email: 'user@example.com' };
    const wrapper = shallow(
      <FirstPage user={elem} />,
    );
    expect(shallowToJson(wrapper)).toMatchSnapshot();
  });
});


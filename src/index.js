import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import configureStore from './store';
import './index.css';
import App from './containers/App';

// Let the reducers handle initial state
// Grab the state from a global variable injected into the server-generated HTML
const initialState = window.__PRELOADED_STATE__;
const store = configureStore(initialState);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>, document.getElementById('root'),
);

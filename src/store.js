import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import reducers from './reducers';

export default function configureStore(initialState = {}) {
  // Create the store with two middlewares
  const middlewares = [
    thunk,
  ];

  const enhancers = [
    applyMiddleware(...middlewares),
  ];
  const store = createStore(
    reducers,
    initialState,
    compose(...enhancers),
  );

  // Extensions
  // store.runSaga = sagaMiddleware.run
  store.asyncReducers = {}; // Async reducer registry

  return store;
}


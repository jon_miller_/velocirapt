import React from 'react';

// import './App.css'

const NoMatch = () =>
  <div>
    Sorry, page not found
  </div>;

export default NoMatch;


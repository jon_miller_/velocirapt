import * as actions from './user';
import * as types from '../types/user';

describe('actions', () => {
  it('should create an action to set a value', () => {
    const payload = 'Finish docs';
    const expectedAction = {
      type: types.SET,
      payload,
    };
    expect(actions.set(payload)).toEqual(expectedAction);
  });
});

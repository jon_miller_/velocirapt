This repository forks from 
https://github.com/ayroblu/ssr-create-react-app-v2

Code has been updated for more recent React conventions.

Just yarn install to get started

Then...
yarn run build
Server side: yarn start:server
Client side: yarn start
Test suite: yarn test